//
//  SLFLoginViewControllers.m
//  SocialLife
//
//  Created by Andrey on 5/14/16.
//  Copyright © 2016 Andrey. All rights reserved.
//

#import "SLFLoginViewControllers.h"

#import <TwitterKit/TwitterKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SLFLoginViewControllers ()

@end

@implementation SLFLoginViewControllers


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


#pragma mark - Action

- (IBAction)vkontaktePressed:(id)sender {
    
}

- (IBAction)twitterPressed:(id)sender {
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
    
}

- (IBAction)instagramPressed:(id)sender {
}

- (IBAction)facebookPressed:(id)sender {
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            NSLog(@"Process error");
            
        } else if (result.isCancelled) {
            NSLog(@"Cancelled");
            
        } else {
            NSLog(@"Logged in");
        }
    }];
}


@end
